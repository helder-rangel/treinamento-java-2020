package revisao.models;

public class Jantar {

	public static void main(String[] args) {

		Pessoa p = new Pessoa("Helder", 89.8);
		Comida c1 = new Comida("feijoada", 1.2);
		Comida c2 = new Comida("Churrasco",2.1);
		
		System.out.println(p.apresentar());
		p.comer(c1);
		
		System.out.println(p.apresentar());
		p.comer(c2);
		System.out.println(p.apresentar());
	}

}
