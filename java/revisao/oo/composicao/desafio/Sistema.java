package revisao.oo.composicao.desafio;

import java.util.List;

public class Sistema {

	public static void main(String[] args) {
		
		
		Compra compra1 = new Compra();
		compra1.adicionarItem("Caneta",1, 100);
		compra1.adicionarItem("notebook", 2000, 1);
		Compra compra2 = new Compra();
		compra2.adicionarItem("Caderno", 10, 10);
		compra2.adicionarItem(new Produto("Impressora", 1000.00),1);
		
		Cliente cliente = new Cliente("Maria J�lia Moraes");
		
		cliente.compras.add(compra1);
		cliente.adicionarCompra(compra2);
		System.out.println(cliente.obterValorTotal());

	}

}
