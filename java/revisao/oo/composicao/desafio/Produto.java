package revisao.oo.composicao.desafio;

public class Produto {
	
	double preco;
	String nome;
	
	public Produto(String nome, double preco) {
		super();
		this.preco = preco;
		this.nome = nome;
	}
}
