package revisao.oo.composicao.desafio;

import java.util.List;

public class ClienteTeste {

	public static void main(String[] args) {
		
		Cliente cliente1 = new Cliente("Roberto");
		
		Item item1 = new Item();
		Item item2 = new Item();
		Compra compra1 = new Compra();
		compra1.add(item1);

		Produto produto1 = new Produto("Caderno", 18.90);
		Produto produto2 = new Produto("Caneta", 7.20);
	}

}
