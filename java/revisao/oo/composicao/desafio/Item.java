package revisao.oo.composicao.desafio;

public class Item {
	
	Produto produto;
	int quantidade;

	public Item() {	}
	
	public Item(Produto produto, int quantidade) {
		this.produto = produto;
		this.quantidade = quantidade;
	}

	@Override
	public String toString() {
		return "Item [produto=" + produto + ", quantidade=" + quantidade + "]";
	}

	
}
